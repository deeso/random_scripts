
import threading, string, zipfile, subprocess, random, time
import sys, ftplib, hashlib, paramiko, subprocess, apsw

COMPLETED = 0
max_threads = 100
tmp_dir = '/tmp/dex_data'

try:
    os.stat(tmp_dir)
except:
    os.mkdir(tmp_dir)

jpath = lambda p, c: os.path.join(p, c)    
tmp_wd_fmt = jpath(tmp_dir, '%s_dex_extract')
get_wd = lambda :  tmp_wd_fmt%str(random.randint(0, 2**32))



def execute_ddx(dst_dir, dex_file):
    cmd = "java -jar /home/dso/ddx.jar -o -d {0} {1}".format(dst_dir, dex_file)
    print u"Executing: {0}".format(cmd)
    proc = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    proc.wait()
    
def process_dex_file(file_path, dst_path):
    print u"Processing: {0}".format(file_path)
    dex_file = os.path.split(file_path)[1]
    dex_name = os.path.splitext(dex_file)
    dex_file_output = os.path.join(dst_path, dex_file_name)
    if not os.path.exists(dex_file_output):
        os.makedirs(dex_file_output)
    
    execute_ddx(dex_file_output, file_path)


get_wd = lambda :  tmp_wd_fmt%str(random.randint(0, 2**32))

def prune_threads(cthreads):
    alive = [i for i in cthreads if i.is_alive()]
    cnt = 0
    while cnt < len(cthreads):
        if not cthreads[cnt] in alive:
            del cthreads[cnt]
        cnt += 1
    del cthreads
    return alive



if __name__ == "__main__":
    file_paths = []
    if len(sys.argv) < 3:
       print "%s <DB_NAME> scan_directory1 [scan_directory2 ..]"%sys.argv[0]
       sys.exit(-1)

    db = sys.argv[1]
    directories = sys.argv[2:]
    print "Searching for apks in:\n%s"%("\n".join(directories))
    print "writing results to db: %s"% db

    for i in directories:
        file_paths += get_files_paths(i)

    file_paths = [i for i in file_paths if i.find("__MACOSX") == -1]
    print "Discovered %d paths."% len(file_paths)
    db_conn = init_sqlite_strings_conn(db)
    processed_paths = get_unique_paths(db_conn)
    #conn.close()
    print "Identified %d unique paths that have already been processed"%len(processed_paths)
    print "Processing %d paths after pruning already processed file paths."% len(file_paths)
    file_paths = [i for i in file_paths if not i in processed_paths]


    k = open("discovered_paths.txt", 'w')
    file_paths.sort()
    k.write("\n".join(file_paths))
    print "Done discovering... Found %d apks."%(len(file_paths))

    print "Processing each of the apks"
    running_threads = []
    cnt = 1
    inserts = 0
    for path in file_paths:
        cnt += 1
        inserts += insert_findings(db_conn)
        if inserts >= 3*max_threads:
            backup_sqlite_strings_conn(db_conn, db)
            inserts = 0;
        while max_threads < len(running_threads):
            print ("looks like more threads than allowed are running: %d"%(len(running_threads)))
            running_threads = prune_threads(running_threads)
            print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
            if max_threads > len(running_threads):
                break
            time.sleep(1)
            inserts += insert_findings(db_conn)
            if inserts >= 2*max_threads:
                backup_sqlite_strings_conn(db_conn, db)
                inserts = 0;
        # Go go multi-threaded
        #process_file_path(path)
        t = threading.Thread(target=process_file_path, args=(path,db_conn))
        t.start()
        running_threads.append(t)

    insert_findings(db_conn)
    while len(running_threads) > 1:
        print ("waiting on %d threads"%(len(running_threads)))
        running_threads = prune_threads(running_threads)
        print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
        if 1 > len(running_threads):
            break
        time.sleep(1)
        
    inserts += insert_findings(db_conn)
    backup_sqlite_strings_conn(db_conn, db)