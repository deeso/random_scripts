import threading, string, zipfile, subprocess, random, os, sqlite3, time
import sys, ftplib, hashlib, paramiko
COMPLETED = 0
max_threads = 100
tmp_dir = '/tmp/dex_data'
try:
    os.stat(tmp_dir)
except:
    os.mkdir(tmp_dir)

jpath = lambda p, c: os.path.join(p, c)    
tmp_wd_fmt = jpath(tmp_dir, '%s_dex_extract')
get_wd = lambda :  tmp_wd_fmt%str(random.randint(0, 2**32))

def clean_up_name(apk_name):
    apk_name = apk_name.replace('.', '_').lower()
    bad = apk_name.translate(None, string.letters + string.digits)
    if len(bad.replace('_','')) == 0:
        return apk_name

    # return the apk_name upto the first special character
    return apk_name.split(bad.replace('_', '')[0])[0]

def insert_mapping(db, apk_name, md5sum):
    item = (apk_name, md5sum)
    conn = sqlite3.connect(db, timeout=(1000 * 30.0))
    present = True
    sql = 'select ApkName from apk_ftp_mapping where MD5Sum = ?'
    cursor = conn.execute(sql, (md5sum, ))
    if cursor.fetchone() is None:
        print "%s not found in the apk_ftp_mapping table."%(md5sum)
        print "%s adding apk_ftp_mapping table."%(apk_name)
        sql = 'insert into apk_ftp_mapping values (?,?)'
        conn.execute(sql, item)
        present = False
    
    sql = 'select ApkName,MD5Sum from apk_ftp_mapping where MD5Sum = ? and ApkName = ?'
    cursor = conn.execute(sql, item)
    if cursor.fetchone() is None:
        sql = 'insert into apk_md5_mapping values (?,?)'
        conn.execute(sql, item)
        conn.commit()
    conn.close()
    return present


def sftp_send_file(db, host, port, user, passwd, apk, lfile, md5sum):
    lpresent = insert_mapping(db, apk, md5sum)
    print "Connecting to the SFTP server %s@%s:%d"%(user, host, port)
    
    transport = paramiko.Transport((host, port))
    transport.connect(username = user, password = passwd)
    sftp =  paramiko.SFTPClient.from_transport(transport)

    rfile = md5sum+'.dex'
    lfile_sz = len(open(lfile).read())
    # if present check to see that the files are somewhat similar
    
    
    if not lpresent: 
        print "%s is being stored as %s"%(lfile, rfile)
        tstat = sftp.put(lfile, rfile)
        if tstat.st_size != lfile_sz:
            print "XXXX Error %s class.dex file not the same size on the remote end"%(apk_name)
        sftp.close()
        transport.close()
        return
    
    rstat = None
    
    try:
        rstat = sftp.stat(rfile)
    except:
        pass

    if (lpresent and rstat is None) or \
       (not rstat is None and (rstat.st_size < lfile_sz)):
        print "Incomplete previous xfr. %s is being stored as %s"%(lfile, rfile)
        tstat = sftp.put(lfile, rfile)
        if tstat.st_size != lfile_sz:
            print "XXXX Error %s class.dex file not the same size on the remote end"%(apk_name)
    else:
        print "%s not uploaded as %s, and not sure why."%(lfile, rfile)
    
    sftp.close()
    transport.close()
    
    


def init_apk_ftp_mapping_table_conn(db):
    conn = sqlite3.connect(db, timeout=(1000 * 30.0))
    sql = "create table if not exists apk_ftp_mapping (ApkName text, MD5Sum VARCHAR NOT NULL PRIMARY KEY)"
    conn.execute(sql)
    sql = "create table if not exists apk_md5_mapping (ApkName text, MD5Sum text)"
    conn.execute(sql)
    conn.commit()
    return conn
    


def prune_threads(cthreads):
    alive = []
    global COMPLETED
    for i in cthreads:
        if i.is_alive():
            alive.append(t)
    cnt = 0
    while cnt < len(cthreads):
        if not cthreads[cnt] in alive:
            COMPLETED += 1
            del cthreads[cnt]
        cnt += 1
    del cthreads
    return alive


def extract_send_classes_dex(db, fname, host, port, user, passwd):
    
    path, apk_fname = os.path.split(fname)
    apk_name, blah = os.path.splitext(apk_fname)
    
    # extract apk classes.dex
    try:
        zfile = zipfile.ZipFile(fname)
    except:
        print "XXXXXX Error: ZipFIle failed to parse the following file: %s"%fname
        return [["ZipFile Error", "ZipFile Error","ZipFile Error"]]
    
    if "classes.dex" in set([i.filename for i in zfile.filelist]):
        wd = get_wd()
        os.mkdir(wd)
        print "Extracting the classes.dex to %s"%wd
        classes_dex = jpath(wd, "classes.dex")
        zfile.extract("classes.dex", path=wd)
        md5sum = hashlib.md5(open(classes_dex).read()).hexdigest()
        print "Checking and sending %s (%s) if necessary."%(apk_name, md5sum)
        sftp_send_file(db, host, port, user, passwd, apk_name, classes_dex, md5sum)
        print "Done cleaning up."
        os.unlink(classes_dex)
        os.rmdir(wd) 
        
def get_files_paths( directory):
    files = [jpath(directory, i) for i in  os.listdir(directory)]
    paths = []
    #print files
    for i in files:
        #print i, os.path.isdir(i)
        #print os.path.splitext( i )[-1], os.path.splitext( i )[-1] == '.apk'
        if os.path.isdir(i):
            paths += get_files_paths( i)
        elif os.path.splitext( i )[-1] == '.apk':
            #print "added path"
            paths.append(i)
    return paths
    
    
user = "ftp_user"
host = "b6actual.thecoverofnight.com"
#passwd = 'ftp_user is me buddy!'
port = 9926
db = "observed_classes_dex.db"


if __name__ == "__main__":
    file_paths = []
    if len(sys.argv) < 7:
        print "%s </path/to/db> <host> <port> <user> <passwd> <path> [ path2 ...]" % sys.argv[0]
        sys.exit(1)
 
    db = sys.argv[1] 
    host, port = sys.argv[2], int(sys.argv[3]) 
    user, passwd = sys.argv[4], sys.argv[5]
    directories = sys.argv[6:]
    init_apk_ftp_mapping_table_conn(db)
    print "Searching for apks in:\n%s"%("\n".join(directories))

    for i in directories:
        file_paths += get_files_paths(i)

    file_paths = [i for i in file_paths if i.find("__MACOSX") == -1]

    k = open("discovered_paths.txt", 'w')
    file_paths.sort()
    k.write("\n".join(file_paths))
    print "Done discovering... Found %d apks."%(len(file_paths))

    print "Processing each of the apks"
    running_threads = []
    for path in file_paths:


        while max_threads < len(running_threads):
            #print (". "%(len(running_threads)))
            g = len(running_threads)
            running_threads = prune_threads(running_threads)
            if len(running_threads) < g:
                print ("Pruned threads, left with: %d"%(len(running_threads)))
                print ("Processed %d files"%COMPLETED)
            if max_threads > len(running_threads):
                break
            time.sleep(.5)
        # Go go multi-threaded
        #process_file_path(path)
        t = threading.Thread(target=extract_send_classes_dex, args=(db, path, host, port, user, passwd))
        t.start()
        running_threads.append(t)

    while len(running_threads) > 1:
        print ("waiting on %d threads"%(len(running_threads)))
        running_threads = prune_threads(running_threads)
        print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
        if 1 > len(running_threads):
            break
        time.sleep(1)

