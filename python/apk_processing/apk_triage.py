import threading, string, zipfile, subprocess, random, os, sqlite3, time 
import sys, apsw



max_threads = 50

lock = threading.Lock()

SHARED_QUEUE = []
SHARED_QUEUE_LOCK = threading.Lock()
               
keywords = {
 # Thads list of advertising
 'MNet':['mnetamerica', 'plade', 'mirajigi'],
 'Adsense':['adsense'],
 'Everbadge':["com/everbadge", 'everbadge'],
 'Flurry':['com/flurry', 'flurry'],
 'Google':["com/google/ads", "com/google/analytics"],
 'Mopub':["com/mopub", 'mopub'],
 'MoboSquare':["com/mobosquare", 'mobosquare'],
 'MoboTap':["com/mobotap", 'dolphin', 'mobotap'],
 'Quclix':["com/quclix", 'quclix', 'queclix'],
 

 'AdColony': ['adcolony'],
 'AdFonic': ['adfonic'],
 'AdIQuity': ['adiquity'],
 'AdKnowledge Super Rewards': ['adknowledge super rewards',
  'adknowledge','srpoints'],
 'AdMarvel': ['admarvel'],
 'AdModa': ['admoda'],
 'AdWhirl': ['adwhirl'],
 'Adlantis': ['adlantis'],
 'Admob': ['admob'],
 'AdsMogo': ['adsmogo'],
 'Adwo': ['adwo'],
 'AirPush': ['airpush'],
 'AppBrain AppLift': ['appbrain applift', 'appbrain', 'applift'],
 'AppLovin': ['applovin'],
 'Burstly': ['burstly'],
 'BuzzCity': ['buzzcity'],
 'Cauly Ads': ['cauly ads', 'cauly.co.kr', 'cauly', ],
 'Chartboost': ['chartboost'],
 'Daum ads': ['daum ads', 'daum'],
 'GetJar': ['getjar'],
 'Greystripe': ['greystripe'],
 'Hunt Mobile Ads': ['hunt mobile ads', 'huntmobileads', 'huntmads', 'hunt'],
 'InMobi': ['inmobi'],
 'Inneractive': ['inneractive', 'inner-active'],
 'Jumptap': ['jumptap'],
 'Komli Mobile (aka ZestAdz)': ['komli mobile (aka zestadz)',
  'komlimobile',
  'zestadz',
   'komli',],
 'Kuguo': ['kuguopush', 'kuguo'],
 'LeadBolt': ['leadbolt'],
 'Madhouse SmartMAD': ['madhouse smartmad', 'madhouse', 'smartmad'],
 'Madvertise': ['madvertise'],
 'MdotM': ['mdotm'],
 'Medialets': ['medialets'],
 'Mediba Admaker': ['mediba admaker', 'ad-maker', 'admaker', ],
 'Mediba Ad': ['mediba ad', 'medibaad'],
 'Metaps': ['metaps'],
 'Millennial Media': ['millennial media', 'millennialmedia'],
 'MoPub': ['mopub'],
 'MobClix': ['mobclix'],
 'MobFox': ['mobfox'],
 'MobPartner': ['mobpartner'],
 'MobWIN': ['mobwin'],
 'Moolah Media': ['moolah media', 'moolahmedia','moolah'],
 'mOcean Mobile': ['moceanmobile','mocean'],
 'Nexage': ['nexage'],
 'Noqoush AdFalcon': ['noqoush adfalcon', 'noqoush', 'adfalcon'],
 'Papaya Offer': ['papaya offer', 'papaya', 'papayamobile'],
 'Pontiflex Offers': ['pontiflex offers', 'pontiflex'],
 'Quattro Wireless Ads': ['quattro wireless ads',
  'quattrowireless', 'quattro'],
 'RevMob': ['revmob'],
 'Rhythm Premium Mobile Video Advertising': ['rhythm premium mobile video advertising',
  'rythmnewmedia'],
 'SendDroid': ['senddroid'],
 'Smaato': ['smaato'],
 'Sponsorpay': ['sponsorpay'],
 'Startapp': ['startapp'],
 'Tap for Tap': ['tap for tap', 'tapfortap'],
 'TapIt': ['tapit'],
 'Tapjoy': ['tapjoy'],
 'VDopia': ['vdopia'],
 'VServ': ['vserv'],
 'Vpon': ['vpon'],
 'WAPS': ['waps'],
 'WiYun': ['wiyun'],
 'Wooboo': ['wooboo'],
 'YouMi': ['youmi'],
 'airAD': ['airad'],
 'domob': ['domob'],
 'sellAring': ['sellaring']}

rev_keywords = {}

for k in keywords:
    for v in keywords[k]:
        rev_keywords[v] = k

def queue_findings(item):
    global SHARED_QUEUE, SHARED_QUEUE_LOCK
    print "Queueing findings for %s"%item[0]
    SHARED_QUEUE_LOCK.acquire()
    SHARED_QUEUE.append(item)
    SHARED_QUEUE_LOCK.release()

def insert_findings(db_conn):
    global SHARED_QUEUE, SHARED_QUEUE_LOCK
    cnt = len(SHARED_QUEUE)
    print "Inserting %d results into the database."%cnt
    inserts = cnt
    while cnt > 0:
        SHARED_QUEUE_LOCK.acquire()
        apk_name, apk_table, file_path, results = SHARED_QUEUE.pop()
        SHARED_QUEUE_LOCK.release()
        #print type(apk_name), apk_name
        #apk_name = unicode(apk_name)
        
        #print u"Creating a table for %s, if it does not exist for %s"%(unicode(apk_table, 'utf-8'),
        #                     unicode(apk_name, 'utf-8'))
        create_apk_table(db_conn, apk_table)
    
        #print u"Inserting %d results for %s into "%(len(results),
        #    file_path), apk_name
        cnt -= 1
        for result in results: 
            AdLibrary, SearchString, MatchedString = result
            insert_string_find(db_conn, apk_table, apk_name, file_path, SearchString,
                               AdLibrary, MatchedString)
    
    print "Completed results insertion into the database."
    return inserts    

def get_unique_paths(conn):
    paths = set()
    apk_tables_sql = "select name from sqlite_master where type = 'table';"
    apk_paths = "SELECT DISTINCT Path FROM [%s];"
    build_str_fmt = lambda x: apk_paths%x

    cursor = conn.cursor().execute(apk_tables_sql)
    tables = [i[0] for i in cursor]
    stmts = map(build_str_fmt, tables)
    
    for stmt in stmts:
        cursor = conn.cursor().execute(stmt)
        paths |= set([i[0] for i in cursor])
    return paths


def init_sqlite_strings_conn(db):
    new_conn = apsw.Connection(db)
    new_memcon = apsw.Connection(":memory:")
    with new_memcon.backup("main", new_conn, "main") as backup:
        backup.step() # copy whole database in one go
    return new_memcon
    
def backup_sqlite_strings_conn(db_conn, db):
    new_conn = apsw.Connection(db)
    with new_conn.backup("main", db_conn, "main") as backup:
        backup.step() # copy whole database in one go
    

def create_apk_table(db_conn,  apk_table):
    #conn = init_sqlite_strings_conn(db)
    sql = u"create table if not exists [{0}] (ApkName text, Path text,Keyword text, LibraryMatch text, StringMatch text)".format(apk_table)
    db_conn.cursor().execute(sql)
    
def insert_string_find(db_conn, ApkTable, ApkName, Path, Keyword, LibraryMatch, StringMatch):
    item = (ApkName, unicode(Path, 'utf-8'), Keyword, LibraryMatch, StringMatch)
    sql = u'insert into [{0}]  values (?,?,?,?, ?)'.format(ApkTable)
    db_conn.cursor().execute(sql, item)

def clean_up_name(apk_name):
    
    apk_name = u'['+apk_name+u']'
    #bad = apk_name.translate(None, string.letters + string.digits)
    #if len(bad.replace('_','')) == 0:
    #    return apk_name
    #
    ## return the apk_name upto the first special character
    #return apk_name.split(bad.replace('_', '')[0])[0]
    return apk_name
    
jpath = lambda p, c: os.path.join(p, c)


tmp_dir = '/tmp'
tmp_wd_fmt = jpath(tmp_dir, '%s_apk_wd')


get_wd = lambda :  tmp_wd_fmt%str(random.randint(0, 2**32))

def prune_threads(cthreads):
    alive = []
    for i in cthreads:
        if i.is_alive():
            alive.append(t)
    cnt = 0
    while cnt < len(cthreads):
        if not cthreads[cnt] in alive:
            del cthreads[cnt]
        cnt += 1
    del cthreads
    return alive

def run_strings_cmd(fname):
    cmd = "strings %s"%fname
    proc = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    out = ""
    try:
        tmp, in_ = proc.communicate()
        out = tmp
    except (OSError, IOError) as e:
        return out
    
    while True:
        tmp = ""
        try:
            tmp, in_ = proc.communicate()
        except:
            break

        if tmp is None:
            break
        out += tmp
    
    return out


def brute_force_string_search(keywords, strings):
    results = []
    found_one = False    
    for i in keywords:
        if found_one:
            break

        search_strings = keywords[i]
        for s in search_strings:
            if found_one:
                break
            elif strings.find(s) > -1:
                results.append([i, s, strings])
                found_one = True
        
    
    return results
    

def perform_strings_check(fname):
    wd = get_wd()
    os.mkdir(wd)
    results = []
    print "Extracting strings from the zip filenames: %s "%(fname)
    # extract apk classes.dex
    classes_strings = ""
    try:
        zfile = zipfile.ZipFile(fname)
    except:
        print "XXXXXX Error: ZipFIle failed to parse the following file: %s"%fname
        os.rmdir(wd)    
        return [["ZipFile Error", "ZipFile Error","ZipFile Error"]]

    filenames = set([i.filename for i in zfile.filelist])
    if "classes.dex" in filenames:
        print "Extracting the classes.dex to %s"%wd
        classes_dex = jpath(wd, "classes.dex")
        zfile.extract("classes.dex", path=wd)
        print "Running strings on the classes.dex file"
        classes_strings = run_strings_cmd(classes_dex)
        os.unlink(classes_dex)
        print "Deleting the classes_dex."
    os.rmdir(wd)    
    print "Removing the tmp dir."
    
    print "Scanning the zipped filenames for strings of interest"
    results = []
    for f in filenames:
        results += brute_force_string_search(keywords, f)
    
    if len(classes_strings) == 0:
        return results
    
    
    print "Scanning the classes.dex strings for strings of interest"
    strings = classes_strings.splitlines()
    for s in strings:
        results += brute_force_string_search(keywords, s)
    
    print "Done groping the %s string content"%fname
    return results
    
    
def process_file_path(file_path, db_conn):
    print "Started processing: %s"%file_path
    path, apk_fname = os.path.split(file_path)
    apk_name, blah = os.path.splitext(apk_fname)
    apk_name = unicode(apk_name, 'utf-8')
    apk_table = apk_name.replace('[', '_').replace(']', '_')
    results = perform_strings_check(file_path)
    
    if len(results) == 0:
        results.append(( '', '', ''))

    
    queue_findings((apk_name, apk_table, file_path, results))    
    print "Done processing %s"%file_path     
    
def get_files_paths( directory):
    files = [jpath(directory, i) for i in  os.listdir(directory)]
    paths = []
    #print files
    for i in files:
        #print i, os.path.isdir(i)
        #print os.path.splitext( i )[-1], os.path.splitext( i )[-1] == '.apk'
        if os.path.isdir(i):
            paths += get_files_paths( i)
        elif os.path.splitext( i )[-1] == '.apk':
            #print "added path"
            paths.append(i)
    return paths




if __name__ == "__main__":
    file_paths = []
    if len(sys.argv) < 3:
       print "%s <DB_NAME> scan_directory1 [scan_directory2 ..]"%sys.argv[0]
       sys.exit(-1)

    db = sys.argv[1]
    directories = sys.argv[2:]
    print "Searching for apks in:\n%s"%("\n".join(directories))
    print "writing results to db: %s"% db
    
    for i in directories:
        file_paths += get_files_paths(i)
    
    file_paths = [i for i in file_paths if i.find("__MACOSX") == -1]
    print "Discovered %d paths."% len(file_paths)
    db_conn = init_sqlite_strings_conn(db)
    processed_paths = get_unique_paths(db_conn)
    #conn.close()
    print "Identified %d unique paths that have already been processed"%len(processed_paths)
    print "Processing %d paths after pruning already processed file paths."% len(file_paths)
    
    file_paths = [i for i in file_paths if not i in processed_paths]
    
    
    k = open("discovered_paths.txt", 'w')
    file_paths.sort()
    k.write("\n".join(file_paths))
    print "Done discovering... Found %d apks."%(len(file_paths))
    
    print "Processing each of the apks"
    running_threads = []
    cnt = 1
    inserts = 0
    for path in file_paths:
        cnt += 1
        inserts += insert_findings(db_conn)
        if inserts >= 3*max_threads:
            backup_sqlite_strings_conn(db_conn, db)
            inserts = 0;
        while max_threads < len(running_threads):
            print ("looks like more threads than allowed are running: %d"%(len(running_threads)))
            running_threads = prune_threads(running_threads)
            print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
            if max_threads > len(running_threads):
                break
            time.sleep(1)
            inserts += insert_findings(db_conn)
            if inserts >= 2*max_threads:
                backup_sqlite_strings_conn(db_conn, db)
                inserts = 0;
        # Go go multi-threaded
        #process_file_path(path)
        t = threading.Thread(target=process_file_path, args=(path,db_conn))
        t.start()
        running_threads.append(t)
    
    insert_findings(db_conn)
    while len(running_threads) > 1:
        print ("waiting on %d threads"%(len(running_threads)))
        running_threads = prune_threads(running_threads)
        print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
        if 1 > len(running_threads):
            break
        time.sleep(1)

    inserts += insert_findings(db_conn)
    backup_sqlite_strings_conn(db_conn, db)
#for f in file_paths:
#    print perform_strings_check(f)




