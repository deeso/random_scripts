# -*- coding: UTF-8 -*-
import threading, string, zipfile, subprocess, random, os, apsw, time
import sys, ftplib, hashlib, paramiko
COMPLETED = 0
max_threads = 100
tmp_dir = '/research_data/tmp/dex_data'
try:
    os.stat(tmp_dir)
except:
    os.mkdir(tmp_dir)

jpath = lambda p, c: os.path.join(p, c)    
tmp_wd_fmt = jpath(tmp_dir, u'%s_dex_extract')
get_wd = lambda :  tmp_wd_fmt%str(random.randint(0, 2**32))


new_memcon = apsw.Connection(":memory:")



def insert_mapping(db_conn, apk_name, md5sum, capture_date, path, rfile):
    item = (apk_name, md5sum, capture_date, path, rfile)
    sql = 'insert into apk_md5_mapping values (?,?,?, ?, ?)'
    db_conn.cursor.execute(sql, item)

    
def copy_file(db, dst_dir, apk, capture_date, path, lfile, md5sum):
    rfile = os.path.join(dst_dir, md5sum+u'.dex')
    insert_mapping(db, apk, md5sum, capture_date, path, rfile)
    
    lfile_sz = len(open(lfile).read())
    # if present check to see that the files are somewhat similar
    print u"%s is being stored as %s"%(lfile, rfile)
    open(rfile, 'wb').write(open(lfile, 'rb').read())
    

    


def init_apk_ftp_mapping_table_conn(db_conn):
    sql = "create table if not exists apk_md5_mapping (ApkName text, MD5Sum text, CaptureDate text, OriginalPath text, StoragePath text)"
    db_conn.cursor().execute(sql)
    


def prune_threads(cthreads):
    alive = []
    global COMPLETED
    for i in cthreads:
        if i.is_alive():
            alive.append(t)
    cnt = 0
    while cnt < len(cthreads):
        if not cthreads[cnt] in alive:
            COMPLETED += 1
            del cthreads[cnt]
        cnt += 1
    del cthreads
    return alive


def extract_send_classes_dex(db, dst_dir, fname, capture_date):
    
    path, apk_fname = os.path.split(fname)
    apk_name, blah = os.path.splitext(apk_fname)
    
    # extract apk classes.dex
    try:
        zfile = zipfile.ZipFile(fname)
    except:
        print "XXXXXX Error: ZipFIle failed to parse the following file: %s"%fname
        return [["ZipFile Error", "ZipFile Error","ZipFile Error"]]
    
    if "classes.dex" in set([i.filename for i in zfile.filelist]):
        wd = get_wd()
        os.mkdir(wd)
        print "Extracting the classes.dex to %s"%wd
        classes_dex = jpath(wd, "classes.dex")
        zfile.extract("classes.dex", path=wd)
        md5sum = hashlib.md5(open(classes_dex).read()).hexdigest()
        #print u"Checking and sending %s (%s) if necessary."%(apk_name, md5sum)
        copy_file(db, dst_dir, apk_name, capture_date, path, classes_dex, md5sum )
        print u"Done cleaning up."
        os.unlink(classes_dex)
        os.rmdir(wd) 
        
def get_files_paths( directory):
    files = [jpath(directory, i) for i in  os.listdir(directory)]
    paths = []
    #print files
    for i in files:
        #print i, os.path.isdir(i)
        #print os.path.splitext( i )[-1], os.path.splitext( i )[-1] == '.apk'
        if os.path.isdir(i):
            paths += get_files_paths( i)
        elif os.path.splitext( i )[-1] == u'.apk':
            #print "added path"
            paths.append(i)
    return paths
    
    
db = "observed_classes_dex.db"


if __name__ == "__main__":
    file_paths = []
    if len(sys.argv) < 4:
        print "%s </path/to/db>  <dst_path> [ path2 ...]" % sys.argv[0]
        sys.exit(1)
 
    db = sys.argv[1] 
    dst_dir = sys.argv[2]
    directories = sys.argv[3:]
    init_apk_ftp_mapping_table_conn(db)
    print "Searching for apks in:\n%s"%("\n".join(directories))

    for i in directories:
        file_paths += get_files_paths(i)

    file_paths = [i for i in file_paths if i.find("__MACOSX") == -1]

    #k = open("discovered_paths.txt", 'w')
    file_paths.sort()
    #k.write("\n".join(file_paths))
    print "Done discovering... Found %d apks."%(len(file_paths))

    print "Processing each of the apks"
    running_threads = []
    for path in file_paths:


        while max_threads < len(running_threads):
            #print (". "%(len(running_threads)))
            g = len(running_threads)
            running_threads = prune_threads(running_threads)
            if len(running_threads) < g:
                print ("Pruned threads, left with: %d"%(len(running_threads)))
                print ("Processed %d files"%COMPLETED)
            if max_threads > len(running_threads):
                break
            time.sleep(.5)
        # Go go multi-threaded
        #process_file_path(path)
        t = threading.Thread(target=extract_send_classes_dex, args=(db, dst_dir, path))
        t.start()
        running_threads.append(t)

    while len(running_threads) > 1:
        print ("waiting on %d threads"%(len(running_threads)))
        running_threads = prune_threads(running_threads)
        print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
        if 1 > len(running_threads):
            break
        time.sleep(1)

