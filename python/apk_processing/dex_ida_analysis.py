import apsw, os, shutil, subprocess, threading, time

jpath = lambda p, c: os.path.join(p, c)

db1 = "/research_data/dso_triage/advertising_2013_1Mplus.db"
db2 = "/research_data/dso_triage/advertising_2013_5Mplus.db"
db3 = "/research_data/dso_triage/advertising_2011.db"

failed_copies = open("failed_copies.txt", 'wb')

def execute_ida_analysis(file_path):
    cmd = r'wine start "C:\\Program Files (x86)\\IDA 6.4\\idaq.exe" -A -c -B {0}'.format(file_path)
    print u"Executing: {0}".format(cmd)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    out = ""
    try:
        tmp, in_ = proc.communicate()
        out = tmp
    except (OSError, IOError) as e:
        return out
    
    while True:
        tmp = ""
        try:
            tmp, in_ = proc.communicate()
        except:
            break

        if tmp is None:
            break
        out += tmp
    
    
    proc.wait()
    return out
    
    
    


def process_with_ida(file_path, dst_dir):
    dir, fname = os.path.split(file_path)
    new_dir, ext = os.path.splitext(fname)
    new_dir = jpath(dst_dir, new_dir)
    print "Creating dir {0}".format(new_dir)
    try:
        os.makedirs(new_dir)
    except OSError:
        pass
        
    new_file_path = jpath(new_dir, fname)
    print "IDA will analyse {0}".format(new_file_path)
    shutil.copy(file_path, new_file_path)
    
    execute_ida_analysis(new_file_path)
    
    
    
def get_db_tables(db_conn):
    tcursor = db_conn.cursor().execute('''SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;''')
    tables = [i[0] for i in tcursor]
    return tables






def get_files_interested_files(db):
    conn = apsw.Connection(db)
    tables = get_db_tables(conn)
    sql = "select DISTINCT apk_id from [{0}]"
    tables = [i for i in tables if i != u'apk_md5_mappings']

    results = {}
    for table in tables:
        r = [i[0] for i in conn.cursor().execute(sql.format(table))]
        results[table] = r

    
    new_results = []
    for k,v in results.items():
        if len(v) > 0:
            new_results.append([k, v[0]])

    csql = "SELECT classes_md5 FROM classesmd5_apk_mappings where apk_id=?"
    classes = []
    for md5, apk_id in new_results:
        cmd5 = conn.cursor().execute(csql, (apk_id,)).next()[0]
        classes.append([apk_id, cmd5])

    return classes

def prune_threads(cthreads):
    alive = [i for i in cthreads if i.is_alive()]
    cnt = 0
    while cnt < len(cthreads):
        if not cthreads[cnt] in alive:
            del cthreads[cnt]
        cnt += 1
    del cthreads
    return alive
    
    
def threaded_the_function(file_paths, the_function, max_threads=100, additional_args = []):
    print additional_args
    print tuple(additional_args + ['blah'])
    running_threads = []
    for path in file_paths:
        while max_threads < len(running_threads):
            print ("looks like more threads than allowed are running: %d"%(len(running_threads)))
            running_threads = prune_threads(running_threads)
            print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
            if max_threads > len(running_threads):
                break
            time.sleep(1)
        # Go go multi-threaded
        t = threading.Thread(target=the_function, args=tuple([path] + additional_args))
        t.start()
        running_threads.append(t)
    
    while len(running_threads) > 1:
        print ("waiting on %d threads"%(len(running_threads)))
        running_threads = prune_threads(running_threads)
        print ("Pruned the completed threads, left with: %d"%(len(running_threads)))
        if 1 > len(running_threads):
            break
        time.sleep(1)


classes_2013 =  get_files_interested_files(db1)
classes_2013 += get_files_interested_files(db2)
classes_2011 =  get_files_interested_files(db3)




output_file = "/research_data/dso_triage/webview_results.txt"
base_dir = "/research_data/dex_classes/jan-2013/"
fname_fmt = "{0}.dex"

files = []
for i,c in classes_2013:
    files.append(jpath(base_dir, fname_fmt.format(c)))    

base_dir = "/research_data/dex_classes/may-2011/"
for i,c in classes_2011:
    files.append(jpath(base_dir, fname_fmt.format(c)))    


base_dir = "/research_data/ida_analysis"

cmd_fmt = ''' C:\Program Files (x86)\IDA 6.4\idaq.exe -B {0}'''
ida_batch_cmd = []

cnt = 0
for file_path in files:
    dir, fname = os.path.split(file_path)
    new_dir, ext = os.path.splitext(fname)
    new_dir = jpath(base_dir, new_dir)
    print "Creating dir {0}".format(new_dir)
    new_file_path = jpath(new_dir, fname)
    try:
        os.makedirs(new_dir)
    except OSError:
        pass
    
    try:
        os.stat(new_file_path)
    except:
        try:
            shutil.copy(file_path, new_file_path)
        except:
            print "Failed to copy file: ",new_file_path
            failed_copies.write(new_file_path+'\n')
       
    ida_batch_cmd.append(cmd_fmt.format(new_file_path))

failed_copies.close()
print "Writing the ida_analysis.bat file"    
bat_file = open("ida_analysis.bat", 'wb')
#bat_file.write('@echo on\n')
bat_file.write("\n".join(ida_batch_cmd))
bat_file.close()

#print "Running the multi-threaded analysis"
#threaded_the_function(files, process_with_ida, max_threads=10, additional_args = [base_dir])

cmd = r'wineconsole --backend=curses ida_analysis.bat'
print u"Executing: {0}".format(cmd)
proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
proc.wait()
