import subprocess, sys, os, shutil

quick_cpy = lambda code_dir, dst_dir, path: shutil.copy(jpath(code_dir, path), jpath(dst_dir, path))
jpath = lambda p, c: os.path.join(p, c)

    
    
def get_modified_files(dir, br1, br2):
    cmd_fmt = "cd {0}; git diff --name-only {1} {2}"
    cmd = cmd_fmt.format(dir, br1, br2)    
    print "Performing the following git cmd"
    out = ""
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    try:
        tmp, in_ = proc.communicate()
        out = tmp
    except (OSError, IOError) as e:
        return out
    
    while True:
        tmp = ""
        try:
            tmp, in_ = proc.communicate()
        except:
            break

        if tmp is None:
            break
        out += tmp
    
    return [i.strip() for i in out.splitlines()]

def copy_git_files(code_dir, dst_dir, br1, br2):
    files = get_modified_files(code_dir, br1, br2)
    for f in files:
        print "Copying the following file\n\t {0} to\n\t{1}".format(jpath(code_dir, f), jpath(dst_dir, f))
        quick_cpy(code_dir, dst_dir, f)
        
if __name__ == "__main__":
    args = sys.argv
    if len(args) < 4:
        print "%s <group_dir> <dst_dir> <branch1> <branch2> "%sys.argv[0]
        print "-- quickly copy source from student git dir to a working project dir"
        sys.exit(-1)
    #code_dir = jpath(args[1], "webgoat-assignments")
    copy_git_files(args[1], args[2], args[3], args[4])
     