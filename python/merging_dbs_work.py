import apsw, os, 






def insert_md5_mappings(db_conn, apk_name, apk_path, apk_md5, classes_md5, capture_period):
    apk_md5_mappings_item = (apk_name, unicode(apk_path, 'utf-8'), capture_period )
    sql = "INSERT INTO apk_md5_mappings VALUES(NULL, ?, ?, ? );"
    db_conn.cursor().execute(sql, apk_md5_mappings_item)
    apk_id = db_conn.last_insert_rowid()
    sql = "INSERT INTO classesmd5_apk_mappings VALUES(?, ?);"
    db_conn.cursor().execute(sql, (classes_md5, apk_id))
    sql = "INSERT INTO apkmd5_apk_mappings VALUES(?, ?);"
    db_conn.cursor().execute(sql, (apk_md5, apk_id))
    return apk_id

def insert_string_find(db_conn, ApkTable, ApkId, Category, Keyword, StringMatch):
    item = (ApkId, Category, Keyword, StringMatch)
    sql = u'insert into [{0}]  values (?,?,?, ?)'.format(ApkTable)
    db_conn.cursor().execute(sql, item)
    
def create_apk_table(db_conn,  apk_table):
    sql = u"create table if not exists [{0}] ( apk_id INTEGER, category TEXT, keyword TEXT, matched_string TEXT, FOREIGN KEY(apk_id) REFERENCES apk_md5_mappings(id) )".format(apk_table)
    db_conn.cursor().execute(sql)

def merge_apk_md5_mappings(src, dst):
   id_mappings = {}
   sql = "SELECT id, name, path, capture FROM apk_md5_mappings"
   csql = "SELECT classes_md5 FROM classesmd5_apk_mappings where apk_id=?"
   asql = "SELECT apk_md5 FROM apkmd5_apk_mappings where apk_id=?"   
   apk_mappings_cursor = src.cursor().execute(sql)
   for v in apk_mappings_cursor:
       id, name, path, capture = v
       classes_md5 = src.cursor().execute(csql, (id,))
       apk_md5 = src.cursor().execute(asql, (id,))
       nid = insert_md5_mappings(dst, name, path, apk_md5, classes_md5, capture
       id_mappings[id] = nid
   
   return id_mappings
   
def merge_result_mappings(src, dst, tables, id_mappings):
   
   sql = "SELECT id, category, keyword, stringmatch from [{0}]"
   for table in tables:
       create_apk_table(dst, table)
       cursor = src.cursor().execute(sql.format(table))
       for id, category, keyword, stringmatch in cursor:
           nid = id_mappings[id]
           insert_string_find(dst, table, nid, category, keyword, stringmatch)
       
def backup_sqlite_conn(db_conn, db):
    new_conn = apsw.Connection(db)
    with new_conn.backup("main", db_conn, "main") as backup:
        backup.step() # copy whole database in one go

def merge_memory_dbs(src, dst, tables):
    id_mappings = merge_apk_md5_mappings(src, dst)
    merge_result_mappings(src, dst, tables, id_mappings)

    
def get_db_tables(db_conn):
    tcursor = mem_conn2.cursor().execute('''SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;''')
    tables = [i[0] for i in tcursor if i[0] != "apk_md5_mappings"]
    return tables
    
def read_db_to_memory(db):
    conn = apsw.Connection(db)
    mem_conn = apsw.Connection(":memory:")    
    with mem_conn.backup("main", conn, "main") as backup:
        backup.step()
    conn.close()
    return mem_conn
    
db1 = "/research_data/dso_triage/advertising_2011.db"
db2 = "/research_data/dso_triage/advertising_2013_1Mplus.db"
db3 = "/research_data/dso_triage/advertising_2013_5Mplus.db"

db_out = "/research_data/dso_triage/apk_data_store.db"

mem_conn1 = read_db_to_memory(db1)
mem_conn2 = read_db_to_memory(db2)
mem_conn3 = read_db_to_memory(db3)

tables = get_db_tables(mem_conn1)
merge_memory_dbs(mem_conn1, mem_conn2, tables)
backup_sqlite_conn(mem_conn2, db_out)

tables = get_db_tables(mem_conn3)
merge_memory_dbs(mem_conn3, mem_conn2, tables)
backup_sqlite_conn(mem_conn2, db_out)
