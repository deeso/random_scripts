import datetime, traceback
import os
import volatility.win32.tasks as tasks
import volatility.conf as conf
import volatility.registry as registry
import volatility.commands as commands
import volatility.win32.network as network
import volatility.utils as utils
import volatility.addrspace as addrspace
from pymongo import Connection
import sys
from optparse import OptionParser

# requires stuffs to initialize volatility
registry.PluginImporter()
config = conf.ConfObject()
registry.register_global_options(config, commands.Command)
registry.register_global_options(config, addrspace.BaseAddressSpace)
# default config 
base_conf = {'profile': '', 
    'use_old_as': None, 
    'kdbg': None, 
    'help': False, 
    'kpcr': None, 
    'tz': None, 
    'pid': None, 
    'output_file': None, 
    'physical_offset': None, 
    'conf_file': None, 
    'dtb': None, 
    'output': None, 
    'info': None, 
    'location': '', 
    'plugins': None, 
    'debug': None, 
    'cache_dtb': True, 
    'filename': None, 
    'cache_directory': None, 
    'verbose': None, 
    'write':False,
    'regexp':None,
}

for k,v in base_conf.items():
    config.update(k, v)


get_timestamp = lambda x: int(os.path.split(x)[-1].split('_')[0])

def get_connections(addr_space):
    return [conn for conn in  network.determine_connections(addr_space)]

def get_process_dict(addr_space):
    '''
    Lifted from the PSTree Class
    '''
    return dict((int(task.UniqueProcessId), task)for task in tasks.pslist(addr_space))   

def get_process_handles(pid_dict):
    pid_handles_dict = {}
    for task in pid_dict.values():
        pid_key = int(task.UniqueProcessId)
        handle_count = int(task.ObjectTable.HandleCount)
        handles = [(handle, handle.get_obj_type()) for handle in task.ObjectTable.handles()]
        pid_handles_dict[pid_key] = handles
    return pid_handles_dict
    
        




def prune_threads(cthreads):
    alive = []
    for i in cthreads:
        if i.is_alive():
            alive.append(t)
    return alive
    
    
def execute_conversion(timestamp, cmd, config):
    print ("executing command %s"%str(cmd_str.split()))
    p = subprocess.Popen(cmd_str.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.wait()


def main(image_dir, db_name, host='127.0.0.1', port=27017, 
         user="", password="", profile='WinXPSP3x86'):

    image_name = db_name
    config.update('profile', profile)
    
    
    files = [os.path.join(image_dir, i) for i in os.listdir(image_dir)]
    files.sort()
    the_file_fmt = "file://%s"
    
    
    
    cmds = registry.get_plugin_classes(commands.Command, lower = True)
    
    
    connection = Connection(host, port)
    db = connection['%s'%image_name]
    files.sort()
    f = files[0]
    
    
    config.update('location', the_file_fmt%f)
    cmd_instance = cmds['imageinfo'](config)
    
    
    imageinfo = {'ImageName':image_name,
        'ImageDescription':'Truncated Virtualbox Elf file',
        'ImageInfo':cmd_instance.get_json(cmd_instance.calculate()),
        'TimeStart':get_timestamp(files[0]),
        'TimeEnd':get_timestamp(files[-1]),
        'ImageCount':len(files)
        }
    
    db.metadata.save(imageinfo)
    
    
    command_to_run = ['printkey', 'apihooks', 'callbacks', 'cmdscan', 'devicetree', 'connscan', 
           'consoles', 'dlllist', 'driverirp', 'driverscan', 'envars', 'filescan',
           'getsids', 'gdt', 'handles',  'idt', 'memmap', 'ssdt', 'mutantscan',
           'sockscan', 'sockets',  'threads', 'pslist', 'psscan', 'timers', 'vadtree']
    
    commands_not_to_run = ['apihooks']
           
    threads_list = []
    
    
    cnt = 1
    start_time = datetime.datetime.now()
    for f in files:
        file_start_time = datetime.datetime.now()
        print ("Processing file: (%d) %s"%(cnt, f))
        timestamp = get_timestamp(f)
        config.update('location', the_file_fmt%f)
        results = {key:{} for key in command_to_run}
        config.update('location', the_file_fmt%f)
        for cmd in command_to_run:
            if cmd in commands_not_to_run:
                continue
                    
            print("Running command: %s"%cmd)
            try:
                cmd_instance = cmds[cmd](config)
                d = cmd_instance.calculate()
                cmd_instance.get_json(d)
            except:
                print('Failed %s command'%cmd)
                traceback.print_exc()
                raise
            print("Finished command: %s"%(cmd))    
        file_end_time = datetime.datetime.now()
        img_collection = db["%04d"%timestamp]
        img_collection.insert(results)
        print ('Processing file,(%d) %s, took %d seconds'%(cnt, f, (file_end_time - file_start_time).seconds))
        cnt += 1
        
    end_time = datetime.datetime.now()
    print ("Completed processing all files in %d seconds."%(end_time - start_time).seconds)



parser = OptionParser()

parser.add_option("-s", "--server", action="store", type="string", 
                  default='127.0.0.1', dest="mongo_host",
                  help="mongo_host to connect too", metavar="HOST")

parser.add_option("-n", "--netport", action="store", type="int", 
                  default=27017, dest="mongo_port",
                  help="mongo_port to connect too", metavar="PORT")

parser.add_option("-d", "--db_name", dest="db_name", type="string",
                  default=None, help="database name to store output in",
                  metavar="DB_NAME")

parser.add_option("-i", "--input_dir", dest="input_dir", type="string",
                  default=None, help="input directory with images", 
                  metavar="IMAGES_DIR")

parser.add_option("-r", "--profile", dest="profile", type="string",
                  default='WinXPSP3x86', help="volatile profile of memory images", 
                  metavar="IMAGE_PROFILE")

parser.add_option("-u", "--user", default='', dest="mongo_user",
                  help="user name for mongo", metavar="USER")

parser.add_option("-p", "--password", default='', dest="mongo_password",
                  help="password for mongo", metavar="USER")



if __name__ == '__main__':
    args = sys.argv
    (options, args) = parser.parse_args(args)
    
    if options.input_dir is None:
        raise Exception("The image directory was not specified")
    
    main( options.input_dir, options.db_name, 
         host=options.mongo_host, port=options.mongo_port, 
         user=options.mongo_user, password=options.mongo_password,
         profile=options.profile) 
    


